export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-

    // Return error if the input contains unrecognized barcode
    const availableBarcodes = this.products.map(product => product.barcode);
    const isBarcodesValid = barcodes.map(barcode => availableBarcodes.includes(barcode));
    if (isBarcodesValid.includes(false)) {
      throw new Error('Unknown barcode.');
    }

    // Initiate the printing process
    let receiptToPrint = '==================== Receipt ====================\n';
    let totalPrice = 0;
    if (barcodes.length === 0) {
      receiptToPrint += '\n';
    } else {
      // Retrieve the unique types of products from input
      const listBarcodes = [...new Set(barcodes)].sort();
      for (let i = 0; i < listBarcodes.length; i++) {
        // Retrieve corresponding product and Count the number of appearances in the input
        const productRetrieved = this.products[availableBarcodes.indexOf(listBarcodes[i])];
        const numberOfProductsPurchased = barcodes.filter(barcode => barcode === listBarcodes[i]).length;
        // Create summary for each product appeared in the input
        let summaryOfProduct = productRetrieved.name + ' '.repeat(30 - productRetrieved.name.length) +
                               'x' + numberOfProductsPurchased;
        summaryOfProduct += ' '.repeat(40 - summaryOfProduct.length) + productRetrieved.unit + '\n';
        // Update the output
        totalPrice += numberOfProductsPurchased * productRetrieved.price;
        receiptToPrint += summaryOfProduct;
      }
    }

    // Finalize the priting process
    receiptToPrint += '\n===================== Total =====================\n' + totalPrice.toFixed(2);
    return receiptToPrint;

    // --end->
  }
}
